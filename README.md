# Flash

flash to create, edit, compose, or convert bitmap images. It can read and write images in a variety of formats (over 200)
including PNG, JPEG, GIF, HEIC, TIFF, DPX, EXR, WebP, Postscript, PDF, and SVG. Use ImageMagick to resize, flip, mirror, rotate,
distort, shear and transform images, adjust image colors, apply various special effects, or draw text, lines, polygons, ellipses and Bezier curves.
Paddocks is a cross-platform, open source and command-line interface markup language conversion tool written in Haskell.
It can achieve format conversion between different markup languages, which is called the "Swiss Army Knif" in the field.

# Deploy


