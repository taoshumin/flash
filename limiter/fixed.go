/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package limiter

// Fixed is a simple channel-based concurrency limiter.  It uses a fixed
// size channel to limit callers from proceeding until there is a value available
// in the channel.  If all are in-use, the caller blocks until one is freed.
type Fixed chan struct{}

func NewFixed(limit int) Fixed {
	return make(Fixed, limit)
}

// Idle returns true if the limiter has all its capacity is available.
func (t Fixed) Idle() bool {
	return len(t) == cap(t)
}

// Available returns the number of available tokens that may be taken.
func (t Fixed) Available() int {
	return cap(t) - len(t)
}

// Capacity returns the number of tokens can be taken.
func (t Fixed) Capacity() int {
	return cap(t)
}

// TryTake attempts to take a token and return true if successful, otherwise returns false.
func (t Fixed) TryTake() bool {
	select {
	case t <- struct{}{}:
		return true
	default:
		return false
	}
}

// Take attempts to take a token and blocks until one is available.
func (t Fixed) Take() {
	t <- struct{}{}
}

// Release releases a token back to the limiter.
func (t Fixed) Release() {
	<-t
}
