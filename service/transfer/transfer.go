/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package transfer

import (
	"context"
	"gitlab.xiadat.com/flash"
	"gitlab.xiadat.com/flash/logger"
	"time"
)

const DefaultCommandTimeOut = 3 * time.Second

var _ flash.TransferService = &service{}

type service struct {
	prepare flash.PrepareService
	logger  flash.Logger
}

// New returns a new TransferS service that provides access to
// convert different document format.
func New(logger flash.Logger) flash.TransferService {
	return &service{
		logger:  logger,
		prepare: &filePath{},
	}
}

func NewMock() flash.TransferService {
	return &service{
		logger:  logger.NewDefault(),
		prepare: &filePath{},
	}
}

func (s *service) Transfer(ctx context.Context, arg *flash.TransferArgs) (*flash.TransferArgs, error) {
	// Prepare file parameters in advance.
	arg = s.prepare.Prepare(arg)

	// convert document files.
	switch arg.Type {
	// HTML
	case flash.Html2Text:
		if err := s.htmlToText(arg); err != nil {
			return nil, err
		}
		return arg, nil
	case flash.HtmlOnline2Text:
		if err := s.htmlOnline2Text(arg); err != nil {
			return nil, err
		}
		return arg, nil
	case flash.HtmlOnline2Html:
		if err := s.htmlOnline2Text(arg); err != nil {
			return nil, err
		}
		return arg, nil
		// Txt
	case flash.Txt2Text:
		if err := s.txt2Text(arg); err != nil {
			return nil, err
		}
		return arg, nil
	case flash.Txt2Rtf:
		if err := s.txt2Rtf(arg); err != nil {
			return nil, err
		}
	case flash.Txt2Tex:
		if err := s.txt2Tex(arg); err != nil {
			return nil, err
		}
	case flash.Txt2Docx:
		if err := s.txt2Docx(arg); err != nil {
			return nil, err
		}
	case flash.Txt2Html:
		if err := s.txt2Html(arg); err != nil {
			return nil, err
		}
	case flash.TxtPdflatex2Pdf:
		if err := s.txtPdflatex2Pdf(arg); err != nil {
			return nil, err
		}
	case flash.TxtWkhtmltopdf2Pdf:
		if err := s.txtWkhtmltopdf2Pdf(arg); err != nil {
			return nil, err
		}
	// Docx
	case flash.Docx2Markdown:
		if err := s.docx2Markdown(arg); err != nil {
			return nil, err
		}
	// Markdown
	case flash.Markdown2Html:
		if err := s.markdown2Html(arg); err != nil {
			return nil, err
		}
	case flash.Markdown2Tex:
		if err := s.markdown2Tex(arg); err != nil {
			return nil, err
		}
	case flash.Markdown2Pdf:
		if err := s.markdown2Pdf(arg); err != nil {
			return nil, err
		}
	// LaTeX
	case flash.LaTeX2Text:
		if err := s.laTeX2Text(arg); err != nil {
			return nil, err
		}
	case flash.LaTeX2Docx:
		if err := s.laTeX2Docx(arg); err != nil {
			return nil, err
		}
	}
	return arg, nil
}

func (s *service) htmlToText(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

func (s *service) htmlOnline2Text(arg *flash.TransferArgs) error {
	args := []string{"-s", "-r", "html", arg.From.DocumentURL, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

func (s *service) txt2Text(arg *flash.TransferArgs) error {
	args := []string{"-s", "-t", "rst", "--toc", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

func (s *service) txt2Rtf(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

func (s *service) txt2Tex(arg *flash.TransferArgs) error {
	args := []string{"-s", "-t", "context", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

func (s *service) txt2Docx(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) txt2Html(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) txtPdflatex2Pdf(arg *flash.TransferArgs) error {
	args := []string{"--pdf-engine=pdflatex", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) txtWkhtmltopdf2Pdf(arg *flash.TransferArgs) error {
	args := []string{"--pdf-engine=wkhtmltopdf", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) docx2Markdown(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-t", "markdown", "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) markdown2Html(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-f", "markdown", "-t", "html", "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) markdown2Tex(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-f", "markdown", "-t", "latex", "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) markdown2Pdf(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-f", "markdown", "-t", "pdf", "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) laTeX2Text(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}

// TODO No test, to be tested
func (s *service) laTeX2Docx(arg *flash.TransferArgs) error {
	args := []string{"-s", arg.From.DocumentFilePath, "-o", arg.To.DocumentFilePath}
	body, err := flash.ExecCommand(DefaultCommandTimeOut, "pandoc", args...)
	if err != nil {
		return err
	}
	arg.To.DocumentByte = body
	return nil
}
