/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package transfer

import (
	"gitlab.xiadat.com/flash"
	"gitlab.xiadat.com/flash/uuid"
	"strings"
)

var _ flash.PrepareService = &filePath{}

type filePath struct{}

func (p *filePath) Prepare(args *flash.TransferArgs) *flash.TransferArgs {
	uid := uuid.New().String()
	fs := strings.Join([]string{uid, p.Type(args)}, ".")
	args.To.DocumentFileName = fs
	args.To.DocumentFilePath = strings.Join([]string{args.To.DocumentFilePath, fs}, "/")
	return args
}

func (p *filePath) Type(args *flash.TransferArgs) string {
	switch args.Type {
	case flash.Html2Text:
		return "text"
	case flash.HtmlOnline2Text:
		return "text"
	case flash.HtmlOnline2Html:
		return "html"
	case flash.Txt2Text:
		return "text"
	case flash.Txt2Rtf:
		return "rtf"
	case flash.Txt2Tex:
		return "tex"
	case flash.Txt2Docx:
		return "docx"
	case flash.Txt2Html:
		return "html"
	case flash.TxtPdflatex2Pdf:
		return "pdf"
	case flash.TxtWkhtmltopdf2Pdf:
		return "pdf"
	case flash.Docx2Markdown:
		return "md"
	case flash.Markdown2Html:
		return "html"
	case flash.Markdown2Tex:
		return "tex"
	case flash.Markdown2Pdf:
		return "pdf"
	case flash.LaTeX2Text:
		return "text"
	case flash.LaTeX2Docx:
		return "docx"
	case flash.Text2Html:
		return "html"
	case flash.TextMathml2Html:
		return "html"
	case flash.TextWebtex2Html:
		return "html"
	case flash.TextMathjax2Html:
		return "html"
	case flash.TextKatex2Html:
		return "html"
	}
	return ""
}
