# Pandoc

Pandoc is a Haskell library for converting from one markup format to another, and a command-line tool that uses this library. It can convert from

---------------

# Link: 
 - https://github.com/jgm/pandoc
 - https://pandoc.org/
 - http://halois.github.io/blog/2016/03/24/intropandoc.html
 - https://github.com/gogap/go-pandoc/blob/master/pandoc/pandoc.go
 
 ---------------
 # Installation
 
#### Mac 

```shell script
$ brew reinstall pandoc
$ pandoc --version
$ pandoc --help
```

#### Liunx && Ubuntu 

```shell script
$ wget https://hackage.haskell.org/package/pandoc-1.17.0.3/pandoc-1.17.0.3.tar.gz
$ tar xvzf pandoc-1.17.0.3.tar.gz
$ cd pandoc-1.17.0.3
```
> sudo apt-get install texlive-latex-base

---------------
# Support 

#### parameters
    - f : 输入文件格式 
    - t : 输出文件格式
    - o : 输出文件路径
    - s : standalone

#### Function

- html
    - pandoc -s example1.html -o example7.text
      - ``1`1111pandoc -s -r html http://www.gnu.org/software/make/ -o example12.text
    - pandoc -s -r html http://www.gnu.org/software/make/ -o example13.html
    - pandoc -N --template=template.tex --variable mainfont="Palatino" --variable sansfont="Helvetica" --variable monofont="Menlo" --variable fontsize=12pt --variable version=2.0 MANUAL.txt --pdf-engine=xelatex --toc -o example14.pdf

- txt
    - text     
        - pandoc -s -t rst --toc MANUAL.txt -o example6.text
    - rtf
        - pandoc -s MANUAL.txt -o example7.rtf
    - tex 
        - pandoc -s MANUAL.txt -t asciidoc -o example28.txt
        - pandoc -s -t context MANUAL.txt -o example11.tex
    - docx 
       - pandoc -s MANUAL.txt -o example29.docx
    - html
        - pandoc -s MANUAL.txt -o example2.html
    - pdf 
        - pandoc --pdf-engine=xelatex MANUAL.txt -o example1.pdf
        - pandoc --pdf-engine=pdflatex MANUAL.txt -o example2.pdf
        - pandoc --pdf-engine=wkhtmltopdf  MANUAL.txt -o example3.pdf
- docx
    - pandoc -s example30.docx -t markdown -o example35.md
    
- markdown 
    - pandoc example35.md -f markdown -t html -s -o example35.html
    - pandoc example35.md -f markdown -t latex -s -o example35.tex
    - pandoc example35.md -f markdown -t pdf -s -o example35.pdf
   
- LaTeX
    - pandoc -s example4.tex -o example5.text
    - pandoc -s math.tex -o example30.docx
    
- text
    - html
        - pandoc math.text -s -o mathDefault.html
        - pandoc math.text -s --mathml  -o mathMathML.html
        - pandoc math.text -s --webtex  -o mathWebTeX.html
        - pandoc math.text -s --mathjax -o mathMathJax.html
        - pandoc math.text -s --katex   -o mathKaTeX.html
        - pandoc code.text -s --highlight-style pygments -o example18a.html
        - pandoc code.text -s --highlight-style kate -o example18b.html
        - pandoc code.text -s --highlight-style monochrome -o example18c.html
        - pandoc code.text -s --highlight-style espresso -o example18d.html
        - pandoc code.text -s --highlight-style haddock -o example18e.html
        - pandoc code.text -s --highlight-style tango -o example18f.html
        - pandoc code.text -s --highlight-style zenburn -o example18g.html
    - xml
        - pandoc MANUAL.txt -s -t opendocument -o example20.xml
    - odt
        - pandoc MANUAL.txt -o example21.odt
    - ebook
        - pandoc MANUAL.txt -o MANUAL.epub
     - Textile reader
        - pandoc -s MANUAL.txt -t textile -o example25.textile
        - pandoc -s example25.textile -f textile -t html -o example26.html  
        
- book
    - pandoc -f docbook -t markdown -s howto.xml -o example31.text 
    
- epub
    - pandoc MANUAL.epub -t plain -o example36.text
---
