/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package transfer

import (
	"gitlab.xiadat.com/flash"
	"testing"
)

func Test_filePath_Prepare(t *testing.T) {
	type args struct {
		args *flash.TransferArgs
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "prepare",
			args: args{
				args: &flash.TransferArgs{
					Type: flash.Html2Text,
					From: flash.Document{},
					To: flash.Document{
						DocumentFilePath: "/etc/data/",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &filePath{}
			got := p.Prepare(tt.args.args)
			if got == nil {
				t.Errorf("Prepare() = %v", got)
			}
		})
	}
}
