/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package transfer

import (
	"context"
	"gitlab.xiadat.com/flash"
	"testing"
)

func Test_service_Transfer(t *testing.T) {
	type args struct {
		ctx context.Context
		arg *flash.TransferArgs
	}
	tests := []struct {
		name    string
		fields  flash.TransferService
		args    args
		wantErr bool
	}{
		{
			name:   "html-text",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.Html2Text,
					From: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/input/example1.html",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "online-html-text",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.HtmlOnline2Text,
					From: flash.Document{
						DocumentURL: "http://www.gnu.org/software/make/",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "online-html-html",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.HtmlOnline2Html,
					From: flash.Document{
						DocumentURL: "http://www.gnu.org/software/make/",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "txt-2-text",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.Txt2Text,
					From: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/input/MANUAL.txt",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "txt-2-rtf",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.Txt2Rtf,
					From: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/input/MANUAL.txt",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "txt-2-tex",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.Txt2Tex,
					From: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/input/MANUAL.txt",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "txt-2-docx",
			fields: NewMock(),
			args: args{
				ctx: context.Background(),
				arg: &flash.TransferArgs{
					Type: flash.Txt2Docx,
					From: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/input/MANUAL.txt",
					},
					To: flash.Document{
						DocumentFilePath: "/home/taoshumin_vendor/workspace/go/src/x6t.io/flash/mock/output/",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.Transfer(tt.args.ctx, tt.args.arg)
			if (err != nil) != tt.wantErr {
				t.Errorf("Transfer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("Transfer() error")
			}
		})
	}
}
