/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package flash

import (
	"context"
	"io"
)

type DocumentType string

const (
	Html2Text          DocumentType = "Html2Text"          // Html2Text (input: html)
	HtmlOnline2Text    DocumentType = "HtmlOnline2Text"    // HtmlOnline2Text (input: html)
	HtmlOnline2Html    DocumentType = "HtmlOnline2Html"    // HtmlOnline2Html (input: html)
	Txt2Text           DocumentType = "Txt2Text"           // Txt2Text (input: txt)
	Txt2Rtf            DocumentType = "Txt2Rtf"            // Txt2Rtf (input: txt)
	Txt2Tex            DocumentType = "Txt2Tex"            // Txt2Tex (input: txt)
	Txt2Docx           DocumentType = "Txt2Docx"           // Txt2Docx (input: txt)
	Txt2Html           DocumentType = "Txt2Html"           // Txt2Html (input: txt)
	TxtPdflatex2Pdf    DocumentType = "TxtPdflatex2Pdf"    // TxtPdflatex2Pdf (input: txt)
	TxtWkhtmltopdf2Pdf DocumentType = "TxtWkhtmltopdf2Pdf" // TxtWkhtmltopdf2Pdf (input: txt)
	Docx2Markdown      DocumentType = "Docx2Markdown"      // Docx2Markdown (input: docx)
	Markdown2Html      DocumentType = "Markdown2Html"      // Markdown2Html (input: markdown)
	Markdown2Tex       DocumentType = "Markdown2Tex"       // Markdown2Tex (input: markdown)
	Markdown2Pdf       DocumentType = "Markdown2Pdf"       // Markdown2Pdf (input: markdown)
	LaTeX2Text         DocumentType = "LaTeX2Text"         // LaTeX2Text (input: LaTeX)
	LaTeX2Docx         DocumentType = "LaTeX2Docx"         // LaTeX2Docx (input: LaTeX)
	Text2Html          DocumentType = "Text2Html"          // Text2Html (input: text)
	TextMathml2Html    DocumentType = "TextMathml2Html"    // TextMathml2Html (input: text)
	TextWebtex2Html    DocumentType = "TextWebtex2Html"    // TextWebtex2Html (input: text)
	TextMathjax2Html   DocumentType = "TextMathjax2Html"   // TextMathjax2Html (input: text)
	TextKatex2Html     DocumentType = "TextKatex2Html"     // TextKatex2Html (input: text)
)

// Logger represents an abstracted structured logging implementation. It
// provides methods to trigger log messages at various alert levels and a
// WithField method to set keys for a structured log message.
type Logger interface {
	Debug(...interface{})
	Info(...interface{})
	Error(...interface{})

	WithField(string, interface{}) Logger

	// Writer Logger can be transformed into an io.Writer.
	// That writer is the end of an io.Pipe and it is your responsibility to close it.
	Writer() *io.PipeWriter
}

type (
	// Document is the input and output args.
	Document struct {
		// DocumentByte is the document's bytes
		DocumentByte []byte `json:"document_byte"`
		// DocumentFilePath is the document's file path.
		DocumentFilePath string `json:"document_file_path"`
		// DocumentFileName is the document's file name.
		DocumentFileName string `json:"document_file_name"`
		// DocumentURL is the document's online url address.
		DocumentURL string `json:"document_url"`
		// StoreURL is the document's store url address.
		StoreURL string `json:"store_url"`
	}

	// TransferArgs is the transfer of format args.
	TransferArgs struct {
		// supported conversion format types, link: https://pandoc.org/demos.html.
		Type DocumentType `json:"type"`
		// From is the input args.
		From Document `json:"from"`
		// To is the output args.
		To Document `json:"to"`
	}

	// TransferService ImageMagick to create, edit, compose, or convert bitmap images. It can read and write images in a variety of formats (over 200)
	// including PNG, JPEG, GIF, HEIC, TIFF, DPX, EXR, WebP, Postscript, PDF, and SVG. Use ImageMagick to resize, flip, mirror, rotate,
	// distort, shear and transform images, adjust image colors, apply various special effects, or draw text, lines, polygons, ellipses and Bezier curves.
	// Paddocks is a cross-platform, open source and command-line interface markup language conversion tool written in Haskell.
	// It can achieve format conversion between different markup languages, which is called the "Swiss Army Knif" in the field.
	TransferService interface {
		// Transfer import and export documents.
		// supported conversion format types.
		Transfer(ctx context.Context, args *TransferArgs) (*TransferArgs, error)
	}

	// PrepareService is the processing data before transfer.
	// modify file name or modify file path.
	PrepareService interface {
		// Prepare processing data before conversion.
		Prepare(args *TransferArgs) *TransferArgs
	}
)

// StoreService is the Storage and retrieval of authentication information.
type StoreService interface {
	// Add a new Document in the DocumentStore.
	Add(ctx context.Context, document Document) error
	// Delete the Document from the DocumentStore.
	Delete(ctx context.Context, document Document) error
	// Get the Document from the DocumentStore.
	Get(ctx context.Context) error
}

// Response is the result of a query api.
type Response interface {
	MarshalJSON() ([]byte, error)
}
