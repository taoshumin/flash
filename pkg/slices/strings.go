/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package slices

import "strings"

// Union combines two string sets.
func Union(setA, setB []string, ignoreCase bool) []string {
	for _, b := range setB {
		if ignoreCase {
			if !ExistsIgnoreCase(setA, b) {
				setA = append(setA, b)
			}
			continue
		}
		if !Exists(setA, b) {
			setA = append(setA, b)
		}
	}
	return setA
}

// Exists checks if a string is in a set.
func Exists(set []string, find string) bool {
	for _, s := range set {
		if s == find {
			return true
		}
	}
	return false
}

// ExistsIgnoreCase checks if a string is in a set but ignores its case.
func ExistsIgnoreCase(set []string, find string) bool {
	find = strings.ToLower(find)
	for _, s := range set {
		if strings.ToLower(s) == find {
			return true
		}
	}
	return false
}

// StringsToBytes converts a variable number of strings into a slice of []byte.
func StringsToBytes(s ...string) [][]byte {
	a := make([][]byte, 0, len(s))
	for _, v := range s {
		a = append(a, []byte(v))
	}
	return a
}
