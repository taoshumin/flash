/*
Copyright 2021 Xiadat IO, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pool_test

import (
	"fmt"
	"gitlab.xiadat.com/flash/pkg/pool"
	"runtime"
	"testing"
)

var timeEncoderPool = pool.NewGeneric(runtime.NumCPU(), func(sz int) interface{} {
	return NewTimeEncoder(1)
})

type TimeEncoder interface {
	Write()
}

type encoder struct {
	sz int
}

func NewTimeEncoder(sz int) TimeEncoder {
	return &encoder{
		sz: sz,
	}
}

func (e *encoder) Write() {
	fmt.Println("Write SZ:", e.sz)
}

func TestNewGeneric_Runtime_CPU(t *testing.T) {
	x := timeEncoderPool.Get(1).(TimeEncoder)
	x.Write()
	defer timeEncoderPool.Put(x)
}
