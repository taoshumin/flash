module gitlab.xiadat.com/flash

go 1.15

require (
	github.com/cespare/xxhash v1.1.0
	github.com/google/go-cmp v0.5.6
	github.com/google/uuid v1.2.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/taoshumin/filesystem v0.0.0-20211031091731-787896fc36ff // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
