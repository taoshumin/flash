package main

import (
	"math/rand"
	"time"
)

var (
	version string
	commit  string
	branch  string
)

func init() {
	// If commit, branch, or build time are not set, make that clear.
	if version == "" {
		version = "dev"
	}
	if commit == "" {
		commit = "none"
	}
	if branch == "" {
		branch = "unknown"
	}
}

func main() {
	rand.Seed(time.Now().Unix())
}
